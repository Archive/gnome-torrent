try:
    import gnomevfs
except ImportError:
    import gnome.vfs
    gnomevfs = gnome.vfs
import os

# A hack that is set to a value that is the largest possible BitTorrent meta
# file. This is passed to get_url to pull the entire (hopefully) meta file into
# memory. I do this to prevent huge files from being loaded into memory.
max_torrent_size = 0x400000 # 4 MB

# From RFC 2396, the regular expression for well-formed URIs
rfc_2396_uri_regexp = r"^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?"

# Makes sure a URI is fully qualified and return the result. Return None if it
# isn't a URI.
def fix_up_uri(path):
    try:
	if os.path.isfile(path):
	    uri = gnomevfs.URI(os.path.abspath(path))
	else:
	    uri = gnomevfs.URI(os.path.abspath(path))
	return str(uri)
    except:
	return None

# Load at most read_bytes from a URI and return the result.
def get_url(uri, read_bytes):
	handle = gnomevfs.open(uri, gnomevfs.OPEN_READ)
	content = ''
	
	try:
	    while len(content) < read_bytes:
		content += handle.read(4096)
	except:
	    pass
	    
	return content

