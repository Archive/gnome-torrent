import pygtk
pygtk.require("2.0")
import gtk
import gtk.glade
import gconf
import gobject
import os
import gettext

_ = gettext.gettext

class Preferences:
    _gconf_base_path = '/apps/gnome-torrent'
    DEFAULT_SAVE_PATH      = { 'path' : _gconf_base_path + '/default_save_path', 
                                'default' : os.path.join(os.path.expanduser("~"), _('Downloads')) 
                            }
    USE_DEFAULT_SAVE_PATH  = { 'path' : _gconf_base_path + '/use_default_save_path', 'default' : True }
    LAST_SAVE_PATH         = { 'path' : _gconf_base_path + '/last_save_path', 'default' : '' }

    def __init__(self):
        self.client = gconf.client_get_default()

    def __getitem__(self, key):
        val = self.client.get(key['path'])
        if not val:
            # The key doesn't exist so let's create it
            self.__setitem__(key, key['default'])
            return key['default']
        else:
            if type(key['default']) == str:
                return val.get_string()
            elif type(key['default']) == bool:
                return val.get_bool()
            else:
                return val.to_string()

    def __setitem__(self, key, val):
        if type(key['default']) == str:
            gval = gconf.Value(gconf.VALUE_STRING)
            gval.set_string(val)
        elif type(key['default']) == bool:
            gval = gconf.Value(gconf.VALUE_BOOL)
            gval.set_bool(val)
        else:
            gval = gconf.Value(gconf.VALUE_STRING)
            gval.set_string(val)
        self.client.set(key['path'], gval)

    def notify_cb(self, sender, args):
        print "*** Gconf notify"
        #FIXME: fix gconf notifications
#        self.emit("setting-changed")

class PreferencesDialog:
    def __init__(self, widgets, prefs):
        self.widgets = widgets
        self.prefs = prefs
        self.dialog = self.widgets.get_widget('prefs_dialog')
        self.widgets.get_widget('last_folder').connect('toggled', self.on_last_folder_group_changed)
        self.widgets.get_widget('custom_folder').connect('toggled', self.on_custom_folder_group_changed)
        self.widgets.get_widget('filechooserbutton').connect('selection_changed', self.on_filechooser_selection_changed)
        self.widgets.get_widget('closebutton').connect('clicked', lambda w: self.dialog.hide())

        self.widgets.get_widget('custom_folder').set_active(self.prefs[Preferences.USE_DEFAULT_SAVE_PATH])
        self.update_fc_button()
            
        self.widgets.get_widget('filechooserbutton').set_current_folder(self.prefs[Preferences.DEFAULT_SAVE_PATH])

    def run(self):
        return self.dialog.run()

    def update_fc_button(self):
        if self.widgets.get_widget('custom_folder').get_active():
            self.widgets.get_widget('filechooserbutton').set_sensitive(True)
        else:
            self.widgets.get_widget('filechooserbutton').set_sensitive(False)

    def on_custom_folder_group_changed(self, widget):
        self.prefs[Preferences.USE_DEFAULT_SAVE_PATH] = widget.get_active()
        self.update_fc_button()

    def on_last_folder_group_changed(self, widget):
        self.prefs[Preferences.USE_DEFAULT_SAVE_PATH] = not widget.get_active()
        self.update_fc_button()

    def on_filechooser_selection_changed(self, widget):
        self.prefs[Preferences.DEFAULT_SAVE_PATH] = widget.get_current_folder()
