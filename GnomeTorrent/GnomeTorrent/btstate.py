# BitTorrent related modules.
import BitTorrent.download, BitTorrent.bencode

# Various system and utility modules.
import os, os.path, threading, sha, sys, time, re

try:
    import gnomevfs
except ImportError:
    import gnome.vfs
    gnomevfs = gnome.vfs

from GnomeTorrent.utils import *
import md5

# Manages a BitTorrent session's state into something consistent.
class BtState:
    # Handles the arguments passed to a BtState at initialization based
    # upon command line arguments.
    class Args:
        def __init__(self, args):
            self.args                  = []
            self.path_origin           = None
            self.path_output           = None
            self.suggested_path_output = None
            self.id                    = ''
            
            # The number of arguments following the current one to ignore.
            ignore = 0
            
            for i in range(0,len(args)):
                # If we're ignoring this argument, skip it.
                if ignore > 0:
                    ignore -= 1
                    continue
                
                if args[i] == '--saveas':
                    # Use the value to know where to save the session.
                    
                    # Ignore the next argument, since we're going to use it now.
                    ignore = 1
                    
                    if i+1 < len(args):
                        self.set_path_output(args[i+1])
                elif args[i] == '--responsefile' or args[i] == '--url':
                    # Use the value to know where the meta
                    # file is located.
                    
                    # Ignore the next argument, since we're
                    # going to use it now.
                    ignore = 1
                    
                    # Convert "--responsefile [path]" into
                    # "--url [uri]" and get a suggested
                    # path_output if possible or needed.
                    if i+1 < len(args):
                        self.set_path_origin(args[i+1])
                else:
                    # Assume any stray argument is the
                    # path_origin as if passed to
                    # --reponsefile or --url if it's a
                    # valid URI.
                    if not self.path_origin:
                        uri = fix_up_uri(args[i])
                        
                        if uri:
			    self.set_path_origin(uri)
            
            # If we couldn't deduce a suggested_path_output, base
            # one upon the name of the meta file.
            if not self.suggested_path_output and self.path_origin:
                try:
                    mo = re.search(rfc_2396_uri_regexp, self.path_origin)
                    
                    self.suggested_path_output = os.path.basename(mo.group(5))
                    
                    if self.suggested_path_output[-len('.torrent'):] == '.torrent':
                        self.suggested_path_output = self.suggested_path_output[:-len('.torrent')]
                except:
                    pass
        
        # If we don't already have a path_output, try to find a
        # suggested_path_output from the data in the meta file.
        def find_suggested_path_output(self):
            self.suggested_path_output = None
            
            if self.path_origin and not self.path_output:
                try:
                    torrent_file = get_url(self.path_origin, max_torrent_size)
                    torrent_info = BitTorrent.bencode.bdecode(torrent_file)
                    
                    self.suggested_path_output = torrent_info['info']['name']
                except:
                    pass
        
        # Set the path_origin and update anything that might depend
        # upon it if it's not already set.
        def set_path_origin(self, path_origin):
            if not self.path_origin:
                self.path_origin = path_origin
                self.args.append('--url')
                self.args.append(fix_up_uri(path_origin))
                # Let's generate an unique identificator for each download
                self.id = md5.new(fix_up_uri(path_origin)).hexdigest()
                
                self.find_suggested_path_output()
            else:
                # Fail silently
                pass
        
        # Set the path_output if it's not already set.
        def set_path_output(self, path_output):
            if not self.path_output:
                self.path_output = path_output
                self.args.append('--saveas')
                self.args.append(path_output)
            else:
                # Fail silently
                pass
    
    def __init__(self, args):
        # BitTorrent module related information
        self.args            = args
        self.size_total      = 0                # Total bytes of the download
        # Local information
        self.path_output     = ''               # The path to which the session is being downloaded.
        # Transfer information
        self.done            = False            # True if the download portion of the session is complete
        self.event           = None             # Event used to flag the BitTorrent module to kill the session
        self.thread          = None             # Thread running the BitTorrent module's download
        self.activity        = None             # What the session is doing at the moment
        self.time_begin      = 0.0              # When the current session began
        self.time_remaining  = 0.0              # Estimated time remaining for the download to complete
        self.dl_rate         = 0.0              # The current rate of download in bytes/sec
        self.dl_amount       = 0                # Bytes downloaded from the current session
        self.dl_pre_amount   = 0                # Bytes downloaded from previous sessions
        self.ul_rate         = 0.0              # The current rate of upload in bytes/sec
        self.ul_amount       = None             # Bytes uploaded from the current session (None for unknown)
        self.ul_pre_amount   = 0                # Bytes uploaded from previous sessions
        self.max_uploads     = 0                # Maximum number of peers to upload to
        self.max_upload_rate = 0.0              # Maximum total bytes/sec to upload at once
        # Implementation information
        self.params          = {}               # Parameters passed from the BitTorrent module
        self.params_pounce   = True             # If current settings still need to be reflected in the session
        self.id              = self.args.id     # An 'unique' idetificator for the download
    
    # Return the corrected-for-multiple-sessions downloaded amount in bytes.
    def get_dl_amount(self):
        if self.activity == 'checking existing file':
            return self.dl_amount
        else:
            return self.dl_amount + self.dl_pre_amount
    
    # Return the corrected-for-multiple-sessions uploaded amount in bytes.
    def get_ul_amount(self):
        if self.ul_amount:
            return self.ul_amount + self.ul_pre_amount
        elif self.ul_pre_amount > 0:
            return self.ul_pre_amount
        else:
            return None
    
    # Pseudo-callback to update state when 'file' BitTorrent callback is called.
    def update_file(self, default, size, saveas, dir):
        self.done       = False
        self.size_total = size
        
        if saveas:
            self.path_output = os.path.abspath(saveas)
        else:
            self.path_output = os.path.abspath(default)
        
        return self.path_output
    
    # Pseudo-callback to update state when 'status' BitTorrent callback is called.
    def update_status(self, dict):
        if not self.done:
            if dict.has_key('downRate'):
                self.dl_rate = float(dict['downRate'])
            
            dl_amount = None
            if dict.has_key('downTotal'):
                dl_amount = long(dict['downTotal'] * (1 << 20))
            elif dict.has_key('fractionDone'):
                dl_amount = long(float(dict['fractionDone']) * self.size_total)
            if dl_amount:
                if dl_amount == 0:
                    self.dl_pre_amount = self.dl_amount
                self.dl_amount = dl_amount
            
            if dict.has_key('timeEst'):
                self.time_remaining = float(dict['timeEst'])
        
        if dict.has_key('upRate'):
            self.ul_rate = float(dict['upRate'])
        
        if dict.has_key('upTotal'):
            self.ul_amount = long(dict['upTotal'] * (1 << 20))

        if dict.has_key('activity'):        
            self.activity = dict['activity']

            # Incorporate the previous phase(s) in our download amount.
            self.dl_pre_amount += self.dl_amount
            self.dl_amount = 0
    
    # Pseudo-callback to update state when 'finished' BitTorrent callback is called.
    def update_finished(self):
        self.done = True
        self.dl_amount = self.size_total - self.dl_pre_amount
    
    # Pseudo-callback to update state when 'path' BitTorrent callback is called.
    def update_path(self, path):
        self.path_output = path
    
    # Pseudo-callback to update state when 'param' BitTorrent callback is called.
    def update_param(self, params):
        if params:
            self.params = params
            
            if self.params_pounce:
                self.cap_uploads(self.max_uploads)
                self.cap_upload_rate(self.max_upload_rate)
                
                self.params_pounce = False
        else:
            self.params = {}
    
    # Function to run in another thread.
    def download_thread(self, file, status, finished, error, cols, path, param):
        try:
            # BitTorrent 3.3-style
            BitTorrent.download.download(self.args.args, file, status, finished, error, self.event, cols, path, param)
        except:
            # BitTorrent 3.2-style
            BitTorrent.download.download(self.args.args, file, status, finished, error, self.event, cols, path)
    
    # Start a session with the specified callbacks (which should each call
    # BtState updaters).
    def download(self, file, status, finished, error, cols, path, param, resuming=False):
        self.done          = False
        self.time_begin    = time.time()
        self.event         = threading.Event()
        self.file          = file
        self.status        = status 
        self.finished      = finished
        self.error         = error
        self.cols          = cols
        self.path          = path
        self.param         = param
        self.thread        = threading.Thread(None, self.download_thread, 'bt_dl_thread', (self.on_bt_file, self.on_bt_status, self.on_bt_finished, self.on_bt_error, cols, self.on_bt_path, self.on_bt_param))
        self.dl_rate       = 0.0
        self.dl_amount     = 0
        self.dl_pre_amount = 0
        self.ul_rate       = 0.0
        self.params        = None
        self.params_pounce = True


        if resuming:
            if self.ul_amount:
                self.ul_pre_amount += self.ul_amount
        else:
            self.ul_pre_amount = 0
        self.ul_amount = None
        
        self.thread.start()
    
    def on_bt_file(self, default, size, saveas, dir):
        return self.file(self.id, default, size, saveas, dir)

    def on_bt_status(self, dict = {}, fractionDone = None, timeEst = None, downRate = None, upRate = None, activity = None):
        self.status(self.id, dict, fractionDone, timeEst, downRate, upRate, activity)

    def on_bt_finished(self):
        self.finished(self.id)

    def on_bt_error(self, msg):
        self.error(self.id, msg)

    def on_bt_path(self, path):
        self.path(self.id, path)

    def on_bt_param(self, params):
        self.param(self.id, params)
        
    # Try to end the BitTorrent session and wait for it to die before
    # returning.
    def join(self):
        if self.event:
            self.event.set()
            self.event = None
        if self.thread:
            self.thread.join()
            self.thread = None
    
    # Cap the number of peers to which you will upload.
    def cap_uploads(self, uploads):
        self.max_uploads = int(uploads)
        
        if self.params and self.params.has_key('max_uploads'):
            self.params['max_uploads'](self.max_uploads)
            return self.max_uploads
        else:
            return None
    
    # Cap the total bytes/sec of which you will upload.
    def cap_upload_rate(self, upload_rate):
        self.max_upload_rate = upload_rate
        
        if self.params and self.params.has_key('max_upload_rate'):
            self.params['max_upload_rate'](int(self.max_upload_rate * (1 << 10)))
            return int(self.max_upload_rate * (1 << 10))
        else:
            return None


