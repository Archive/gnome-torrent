import socket, threading, os, sys, pwd

class Backend:
    def __init__(self, on_url):
        self.sock_name = "/tmp/gnome-torrent_%s.%s" % (pwd.getpwuid(os.getuid())[0], os.environ['DISPLAY'][1:])
        self.ssock = socket.socket(socket.AF_UNIX)
        self.ssock.bind(self.sock_name)
        self.ssock.listen(4)
        self.on_url = on_url
        self.event         = threading.Event()
        self.acceptthread = threading.Thread(None, self.accept_thread, 'gt_accept_thread')
        self.acceptthread.start()

    def _run(self, csck):
        chunk = '-'
        msg = ''
        while chunk != '':
            chunk = csck.recv(1)
            if chunk == ':':
                size = int(msg)
                msg = csck.recv(size)
                self.on_url(msg)
                chunk = csck.recv(1)
                msg = chunk
            else:
                msg += chunk
        csck.close()
            
    def accept_thread(self):
        while 1:
            csck, addr = self.ssock.accept()
            #threading.Thread(None, self._run, 'gt_serve_thread', (csck,)).start()
	    self._run(csck)

    def join(self):
        if self.event:
            self.event.set()
            self.event = None
        if self.acceptthread:
            self.acceptthread.join(0)
            self.acceptthread = None
        for th in threading.enumerate():
            if th is not threading.currentThread():
                th.join(0)
                th = None
