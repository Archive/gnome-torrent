import pygtk
pygtk.require("2.0")
import gtk
import egg.trayicon
import gettext

_ = gettext.gettext

class TrayIcon(egg.trayicon.TrayIcon):
    STATUS_IDLE = 0
    STATUS_DOWNLOADING = 1
    STATUS_UPLOADING = 2
    STATUS_NEW = 4

    def __init__(self, icondir):
        self.icondir = icondir
        self.status = self.STATUS_IDLE
        self.tasks = {}
        egg.trayicon.TrayIcon.__init__(self, "GnomeTorrent")
        self.eb = gtk.EventBox()
        image = gtk.Image()
        pb = gtk.gdk.pixbuf_new_from_file_at_size("%s/download.png" % self.icondir, 20, 20)
        image.set_from_pixbuf(pb)
        self.eb.add(image)
        self.add(self.eb)
        self.menu = gtk.Menu()
        self.menu_show = gtk.CheckMenuItem(_("Show active downloads"))
        self.menu.add(self.menu_show)
        self.menu_prefs = gtk.ImageMenuItem(gtk.STOCK_PREFERENCES)
        self.menu.add(self.menu_prefs)
        self.menu.add(gtk.SeparatorMenuItem())
        self.menu_quit = gtk.ImageMenuItem(gtk.STOCK_QUIT)
        self.menu.add(self.menu_quit)
        self.menu.show_all()

    def add_task(self, name, title, desc):
        self.tasks[name] = { 'title' : title, 'desc' : desc, 'progress' : 0 }

    def update_task(self, name, progress):
        self.tasks[name][progress] = progress
        if progress == 100:
            self.status |= self.STATUS_NEW
            self.status &= self.status ^ self.STATUS_DOWNLOADING
            for k,v in self.tasks:
                if v['progress'] < 100:
                    self.status |= self.STATUS_DOWNLOADING

